
.. _sect_uncertainty:

Data Uncertainties 
------------------

The data uncertainties, often referred to as the error bars, are
critical in EM inversion since the data misfit and inversion search
directions are both scaled by the data uncertainties. The error bars are
just as important as the data! While most EM data and uncertainties are
estimated as complex numbers in data processing codes, the complex
values are often  transformed into other forms for inversion (e.g.,
real, imaginary, amplitude, phase, log10 amplitude, apparent resistivity
etc). Here we provide a table showing how to correctly scale the data
uncertainties for each data type supported by MARE2DEM. For the
interested reader, we also provide a brief review of the uncertainty
propagations for the various transforms. For more details and discussion
of some of the advantages and disadvantages of the various data
scalings, see [WCK15]_.

+--------------------+----------------------------------------+------------------------------------------------------------------------------------------------+
| Data Type          |   Data                                 |    Standard Error                                                                              |
+====================+========================================+================================================================================================+  
| complex            | :math:`z=x+iy=ae^{i\phi}`              |:math:`\delta z = \sqrt{2} \sigma`                                                              |           
+--------------------+----------------------------------------+------------------------------------------------------------------------------------------------+
| real or imaginary  | :math:`x,y`                            |:math:`\delta x = \delta y = \sigma`                                                            |           
+--------------------+----------------------------------------+------------------------------------------------------------------------------------------------+
| amplitude          | :math:`a = |z|`                        |:math:`\delta a = \sigma`                                                                       |           
+--------------------+----------------------------------------+------------------------------------------------------------------------------------------------+
| log10(amplitude)   | :math:`l = \log_{10}|z|`               |:math:`\delta l=\frac{1}{\ln(10)}\frac{\sigma}{a}`                                              |           
+--------------------+----------------------------------------+------------------------------------------------------------------------------------------------+
|apparent resistivity|:math:`\rho_a =\frac{1}{\omega\mu}|z|^2`|:math:`\delta \rho_a  = \frac{2 \rho_a}{|z|}  \sigma`                                           |           
+--------------------+----------------------------------------+------------------------------------------------------------------------------------------------+
|log10(app.resist.)  |:math:`\log_{10}\rho_a`                 |:math:`\delta \log_{10}\rho_a=\frac{1}{\ln(10)}\frac{\delta \rho_a}{\rho_a}`                    |           
+--------------------+----------------------------------------+------------------------------------------------------------------------------------------------+
| phase              |:math:`\phi= \arctan(y/x)`              |:math:`\delta \phi=\frac{\sigma}{a}=\frac{1}{2}\frac{\delta\rho_a}{\rho_a}`                     |           
+--------------------+----------------------------------------+------------------------------------------------------------------------------------------------+

Note that MARE2DEM inverts phase with units degrees, so multiply the
phase and its uncertainty defined above by :math:`180/\pi`.

Complex Data
============

The complex data  :math:`z = x + i y` can be related to amplitude *a*
and phase :math:`\phi` (in radians)  using Euler's formula:

.. math::
    \begin{eqnarray}
    z = x + i y = a e^{i\phi} = a (\cos \phi + i \sin \phi),
    \end{eqnarray}
    
where 

.. math::
    \begin{eqnarray}
    x &=& a \cos \phi, \\
    y &=& a \sin \phi.
    \end{eqnarray}
    
Likewise, the amplitude and phase can be found from the complex components using

.. math::
    \begin{eqnarray}
    a &=& \sqrt{x^2+y^2}, \\
    \tan \phi &=&  \frac{y}{x}, \\
    \phi &=&  \arctan \frac{y}{x}.
    \end{eqnarray}
    
When computing :math:`\phi`, you should use the :math:`\it atan2(y,x)`
function since it returns the four-quadrant phase whereas the  :math:`\it
atan(y/x)` function is restricted to :math:`-\pi \le \phi \le \pi`.

Also, when dealing with real data, we generally convert phase into
degrees:

.. math::
 \begin{eqnarray} \phi  (degrees)= \frac{180}{\pi} \phi (radians) \end{eqnarray}

Real and Imaginary
==================

For complex data :math:`z = x + i y`  where *x* is the real component
and *y* is the imaginary component, the  standard error  :math:`\sigma`
is generally assumed to be isotropic so that :math:`\sigma = \delta x =
\delta y`. Isotropic error means that *z* has a circle of uncertainty
around it in the complex plane as shown in :numref:`complex_errors`.

 .. _complex_errors:
 
 .. figure::  _static/images/complex_errors.png 
    :align: center
    :width: 80%

    Isotropic uncertainty in the complex plane 
   
For CSEM data, *z* is the complex electric or magnetic field  at a given
frequency. For MT data, *z* is a component of the MT impedance tensor
(or a component of the tipper vector) at a given frequency. In both
cases, complex data *z* is what is output from the EM response
estimation code that process the time series data.

MARE2DEM allows us to invert the data as a complex quantity, but often
we find it more intuitive to look at the data in amplitude and phase
form, or log(amplitude) and phase.  In the case of MT, the apparent
resistivity form is much easier to comprehend visually than the complex
data *z*. When inverting  CSEM and MT responses with a large dynamic
range (i.e. spanning multiple orders of magnitude), there are some
stability advantages to treating the data as log10(amplitude); see
[WCK15]_.

Given a data scaling transform, we have to know the associated data
uncertainty transforms. Suppose  *q*  is the transformed version of the
original complex data *z*. Thus :math:`q \equiv q(x,y)`. For example,
*q* could be the amplitude, phase, log(amplitude), apparent resistivity,
a polarization ellipses parameter, etc. For computing misfits during
modeling and inversion, we need to know the uncertainty :math:`\delta
q`. This can be found using the standard method for linear propagation
of errors, which uses a first-order Taylor's series expansion. Assuming
the variables *x* and *y* are independent with standard error
:math:`\sigma`, the first-order variance propagation formula is 

.. math :: 
    :label: propagation

    \begin{eqnarray} \delta q^2
    =  \left ( \frac{\partial q}{\partial x} \right) ^2 \sigma^2 + \left (
    \frac{\partial q}{\partial y} \right) ^2 \sigma^2. 
    \end{eqnarray}

The sections below show how to apply this for the various data types 
supported in MARE2DEM.


Amplitude
=========
 
The amplitude *a* of complex variable *z* is:

.. math :: 
    \begin{eqnarray}
    a =  |z| = \sqrt{x^2+y^2},
    \end{eqnarray}
    
Thus we have

.. math :: 
    \begin{eqnarray}
    \frac{\partial a}{\partial x} = \frac{x}{a}, \;\;\;\;\;\;\; \frac{\partial a}{\partial y} = \frac{y}{a}, 
    \end{eqnarray}
    
Using eq. :eq:`propagation`, we find

.. math :: 
    \begin{eqnarray}
    \delta a^2 &=&  \left ( \frac{x}{a}\right )^2 \sigma^2 + \left ( \frac{y}{a}\right )^2 \sigma^2 ,\\
      &= &\sigma^2.
    \end{eqnarray}
    
   
The standard error is therefore 

.. math ::  
    \begin{eqnarray}
    \delta a &=&  \sigma .
    \end{eqnarray}

In :numref:`complex_errors` you can see that this result makes
sense since :math:`\delta a =  \sigma = \delta x = \delta y`.

log10 Amplitude
===============

.. warning ::

    The standard error of log scaled data is not the log of the linear  standard error!

Denote the log of the amplitude as variable *l*:

.. math ::  
    \begin{eqnarray}
    l=  \log_{10}|z| = \log_{10}(a)  = \log_{10}\left ( \sqrt{x^2+y^2} \right ).
    \end{eqnarray}
    
Using the chain rule we have

.. math ::  
    \begin{eqnarray} \frac{\partial l}{\partial x} = \frac{ \partial l}{
    \partial a} \frac{\partial a}{\partial x}   = \frac{1}{\ln(10) a}
    \frac{x}{a}  = \frac{ x }{\ln(10) a^2}, 
    \end{eqnarray}

with a similar expression for the *y* derivative.

Using eq. :eq:`propagation`, the variance of *l* is then:

.. math ::  
    \begin{eqnarray} \delta l^2 &=&  \left ( \frac{ x }{\ln(10) \, a^2}
    \right )^2 \sigma^2 + \left ( \frac{  y }{\ln(10)\, a^2} \right )^2
    \sigma^2, \\ &= & \frac{\sigma^2}{\ln(10)^2\, a^2}.
    \end{eqnarray}
    
The standard error of *l* is then:

.. math :: 
    \begin{eqnarray}
    \delta l &=&  \frac{1}{\ln(10)} \frac{\sigma }{a} = 0.4343 \frac{\delta a }{a} .
    \end{eqnarray}
    
This shows that the standard error for the log10 scaled amplitude is
just a scaled version of the relative amplitude error :math:`\frac{\delta a
}{a}`. So if we say the data has 1% error in amplitude, the standard error
of the log10 amplitude is then 0.004343.  

It can also be helpful to think of visualizing the error bars on a log10
scaled plot of the amplitudes. The formula above shows that if the data
have a fixed relative error, the error bars on the plot will all have
the same vertical length, regardless of the data values. 

Apparent Resistivity
====================

The MT apparent resistivity :math:`\rho_a` is found from the impedance :math:`Z = x + i y` as

.. math ::
    \rho_a = \frac{1}{\omega \mu} |z|^2 = \frac{1}{\omega \mu} (x^2+y^2)

Using the chain rule we have

.. math ::  
    \begin{eqnarray} \frac{\partial \rho_a}{\partial x} =  \frac{2 x}{\omega \mu} 
    \end{eqnarray}

and similarly for the *y* derivative. The standard error of :math:`\rho_a` is then:

.. math ::  
    \delta \rho_a  =  2 \frac {\sigma  }{|Z|} \rho_a
    
and the relative error in apparent resistivity is

.. math ::  
    \frac{\delta \rho_a}{\rho_a}  =  2 \frac{\sigma }{|Z|} 
        
which is twice the relative error in the impedance, where the factor of two is due to the apparent resistivity 
being proportional to the square of the impedance magnitude.
 
Phase
=====

The phase :math:`\phi` of *z* in degrees is:

.. math :: 
    \begin{eqnarray}
    \phi = \frac{180}{\pi} \arctan(z) = \frac{180}{\pi} \arctan\left (\frac{y}{x} \right ),
    \end{eqnarray}
    
with partial derivatives

.. math :: 
    \begin{eqnarray}
    \frac{\partial \phi}{\partial x} &=& -\frac{180}{\pi} \frac{1}{1 + \left (\frac{y}{x}\right)^2} \frac{y}{x^2} ,\\ 
    &=& -\frac{180}{\pi} \frac{y}{x^2 + y^2}  . 
    \end{eqnarray}
    
and
 
 .. math :: 
    \begin{eqnarray}
    \frac{\partial \phi}{\partial y}  &=&  \frac{180}{\pi} \frac{x}{x^2 + y^2} .
    \end{eqnarray}

Using eq. :eq:`propagation`, we find

.. math :: 
    \begin{eqnarray}
        \delta  \phi ^2 &=&  \left (-\frac{180}{\pi} \frac{y}{x^2 + y^2} \right )^2  \sigma^2 + \left (\frac{180}{\pi} \frac{x}{x^2 + y^2} \right )^2  \sigma^2, \\
        &=&  \left ( \frac{180}{\pi} \right ) ^2    \frac{\sigma^2}{a^2}  
    \end{eqnarray}
    
The standard error of :math:`\phi` is then:

.. math :: 
    \begin{eqnarray}
    \delta \phi &=& \frac{180}{\pi}  \frac{\sigma }{a} =\frac{180}{\pi}  \frac{\delta a }{a}  
    = \frac{180}{\pi}  \frac{1}{2}\frac{\delta \rho_a}{\rho_a}
     \end{eqnarray}
    
So for 1% relative error in amplitude (:math:`\frac{\delta a }{a}`), the
corresponding phase error :math:`\delta \phi` is then 0.573º.  Note also that the last term on the right
shows the phase error for MT data will be half the relative error in apparent resistivity.

In :numref:`complex_errors` you can see that this result makes
sense, since small changes in  :math:`\phi` will scale with :math:`\frac{\delta a
}{a}`.

A word of warning
=================

The uncertainty propagation analyses above relied on a first-order
Taylor's series expansion that implicitly assumes  :math:`\sigma  <<
|z|`, in other words it assumes that the uncertainty is much smaller
than the data amplitude. The formulas for transformed uncertainties
break down when the uncertainty grows too large. See [WCK15]_ for an
in-depth analysis of the break-down. Hence, data with large errors (say
greater than 50% or so) should generally be omitted from the inverted
data set given issues with propagating the error; furthermore very noisy
data probably doesn't help the inversion resolve conductivity so it can
have little useful value, that is unless the noisy data are all you have
to work with. 


