
.. _sect_settings_file: 

MARE2DEM Settings File
----------------------

Contains parameters that control the adaptive refinement computations
and the parallel decompositions.  
See the Example files for further details. 

