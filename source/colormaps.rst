Colormaps
---------

A wide selection of colormaps are available in MARE2DEM's MATLAB routines. A complete
palette of the currently available colormaps is  shown in the image below. 
We strongly recommend using one of the perceptually uniform colormaps and to avoid using non-uniform colormaps
such as MATLAB's old default colormap *jet*, as its narrow band of yellow and cyan create stripes 
that can significantly bias interpretations. See the examples below.

We also provide the following colormaps for making colorblind accessible plots:

- red-green colour blind (Protanopia/Deuteranopia):

  -       'CBL1' - 'CBL4' 
  -       'CBD1' - 'CBD2' 
  -       'CBC1' - 'CBC2' 

- blue-yellow colour blind (Tritanopia)

  -       'CBTL1' - 'CBTL4'
  -       'CBTD1' 
  -       'CBTC1' - 'CBTC2' 

.. image:: _static/images/colormaps.png
    :width: 100%

.. note::
  Special thanks to Peter Kovesi and Matteo Niccoli for making their colormaps freely available

Consider the following figures, which have the recommended perceptually more uniform colormaps in the left column and 
non-uniform colormaps in the right column. Is that cyan colored stripe in the figure on the upper right the 
top of basement? The figures in the left column with more uniform colormaps suggest it isn't such a sharp looking boundary.

|fig3| |fig1|
|fig4| |fig2|
 
 
   
.. |fig1| image:: _static/images/colormap_jet.png
    :width: 49%
    
.. |fig2|  image:: _static/images/colormap_swtth.png
    :width: 49%
    
.. |fig3|  image:: _static/images/colormap_parula.png
    :width: 49%
    
.. |fig4|  image:: _static/images/colormap_R1.png
    :width: 49%
    
Another option is to use a more subtle colormap with added contour lines:    
    
..  image:: _static/images/colormap_CBL1_contours.png
   :width: 100%






