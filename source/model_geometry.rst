.. _sect_model_geometry:

Model Geometry
--------------

MARE2DEM uses a right-handed coordinate system with the *z* axis
positive down. *x* is the model strike direction where conductivity is
invariant, so :math:`{\sigma} = {\sigma(y,z)}`.
  
.. Note::
    There is no assumption about where :math:`z=0` resides in the model or 
    data spaces, although generally it is recommended to set :math:`z=0` to
    be sea level. Similarly, transmitters and receivers
    can be located at any *x* position, but generally they should be close
    to :math:`x=0`.

.. figure::  _static/images/model_geometry.png 
    :width: 50%

    Cross sectional view of the right-handed coordinate system used in
    MARE2DEM. As the right-hand name implies, if you point your
    right index finger along *x* and your right middle finger along *y*,
    your right thumb will point down along the *z* axis.
     
.. figure::  _static/images/model_geometry_map.png 
    :width: 50%

    Map view of the right-handed coordinate system used in MARE2DEM.

