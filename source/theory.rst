Background Theory
=================



.. toctree::
   :maxdepth: 2
   
   governing_equations
   finite_elements
   inversion
   roughness
   uncertainty
   bounds
   sensitivity
   reciprocity




