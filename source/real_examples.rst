Real Data
=========

The sections below show published real data applications of
MARE2DEM for inverting magnetotelluric and controlled source
electromagnetic data. All figures shown here were made using MARE2DEM's
MATLAB plotting tools, which support various functions such as
interpolated shading, resistivity contours, sensitivity contours,
sensitivity masking, well-log overlays, seismic overlays, a wide array
of color maps, and many other customizable appearance options.  

Note that the seawater and air layers (which are fixed parameters) have been
turned off in the plots for marine models below. Also, only the central
region of interest is shown for each model; the entire model domains
extend 100's or 1000's of km beyond the central region of interest shown
in the figures, where these outer padding regions ensure that model boundary
reflections do not corrupt the finite element solutions in the region of
interest.



East Pacific Rise Mid-Ocean Ridge MT
------------------------------------

 .. figure::  _static/images/examples/EPR_2_ss_tri.32.resistivity_grid.png 
    :width: 80%

    Example of using an unstructured grid of triangular free parameters to
    invert seafloor MT data from the East Pacific Rise spreading ridge
    at 9º 30' N, revealing a conductive region of upper mantle upwelling
    and partial melting. Small triangular parameters are used at the seafloor to
    allow for near-surface variations and to accomodate rugged
    bathymetry, while deeper in the mantle larger triangles are used. Inverted white 
    triangles along the seafloor show the MT receiver locations.
    Modified from: Key, K., Constable, S., Liu, L., & Pommier, A. (2013),
    Electrical image of passive mantle upwelling beneath the northern
    East Pacific Rise. Nature, 495(7442), 499–502, DOI:
    `10.1038/nature11932 <http://doi.org/10.1038/nature11932>`_.

 .. figure::  _static/images/examples/EPR_2_ss_tri.32.resistivity_femesh.png 
    :width: 80%

    Behind the scenes, MARE2DEM generates finite element (FE) meshes for
    the forward calculations of the EM fields. These unstructured
    triangular finite element meshes are automatically generated and
    adaptively refined using a goal-oriented a posteriori error
    estimator for the FE solution. This image shows an example FE mesh
    (thin black lines) near the seafloor for the model above. Notice how
    the FE mesh conform to the free and fixed parameter polygons (thick lines).
    The FE mesh shows the most refinement near the MT stations, where fine meshing
    ensures accurate electromagnetic fields are computed.
    
    
Middle American Trench Offshore Nicaragua MT and CSEM
-----------------------------------------------------


 .. figure::  _static/images/examples/aniso_0p75x.10.resistivity.png 
    :width: 100%
 .. figure::  _static/images/examples/aniso_0p75x.10.resistivity_ratio.png 
    :width: 100%
       
    Example of using an unstructured grid of quadrilateral parameters to
    invert seafloor MT data from the Middle America Trench offshore
    Nicaragua for triaxially anisotropic conductivity, revealing an
    anisotropic conductive  asthenosphere layer consistent with
    partially molten mantle. Upper panel shows the *y* component of
    resistivity and the lower panel shows the *y/x* resistivity
    anisotropy ratio. Modified from: Naif, S., Key, K., Constable, S., &
    Evans, R. L. (2013), Melt-rich channel observed at the
    lithosphere-asthenosphere boundary. Nature, 495(7441), 356–359, DOI:
    `10.1038/nature11939 <http://doi.org/10.1038/nature11939>`_.
 
 
 .. figure::  _static/images/examples/Serpent1.15.resistivity.png
    :width: 100%
 
    Inversion of marine CSEM data crossing the Middle America Trench.
    Shaded colors and contours show resistivity while the gray masked
    region in the seafloor masks areas the data are insensitivity to, as
    determined by the normalized Jacobian matrix (see
    :Ref:`sect_sensitivity`) . Modified from [Key16]_ and Naif, S., Key,
    K., Constable, S., & Evans, R. L. (2016), Porosity and fluid budget
    of a water‐rich megathrust revealed with electromagnetic data at the
    Middle America Trench. Geochemistry Geophysics Geosystems, 17(11),
    4495–4516, DOI: `10.1002/2016GC006556
    <http://doi.org/10.1002/2016GC006556>`_.   

 .. figure::  _static/images/examples/Serpent1.15.sensitivity.png
    :width: 100%
 
    A similar plot to the one above, but now with shaded colors showing
    the log10 sensitivity (see :Ref:`sect_sensitivity`) and contours
    showing log10 resistivity.
     
    
 .. figure::  _static/images/examples/Serpent1.15.response.png
    :width: 100%
 
    Example screen shot of CSEM response plot made with the
    interactive MATLAB code `plotMARE2DEM_CSEM.m`. Upper row: symbols
    show the amplitude and phase response at three frequencies for a
    single receiver along with the corresponding model response (black
    lines). Lower row: normalized residuals for the data fit.

 .. figure::  _static/images/examples/Serpent1.15.misfitbreakdown.png
    :width: 100%
 
    Example screen shot of CSEM misfit breakdown plot made with the
    interactive MATLAB code `plotMARE2DEM_CSEM.m`. The misfit breakdown
    plot is useful for showing how well the data is fit as a function of
    receiver position, transmitter position, frequency, data type and
    transmitter-receiver range. Regions with large misfits can indicate
    where they data may be noisy or have problems, or where the model
    mesh needs to be refined to allow for smaller scale features
    required to fit the data.

Gemini Salt Body, Gulf of Mexico
--------------------------------

 .. figure::  _static/images/examples/Gemini_LineI_TE.6.resistivity.png
    :width: 100%
 
    Inversion of marine MT data from Line I at Gemini Prospect, Gulf of
    Mexico. This inversion relaxed the roughness penalty along the
    seismically imaged top of the Gemini salt body (white line),
    allowing for a sharp jump in inverted resistivity. Penalty cuts like
    this are easily created in MARE2DEM's graphical user interface
    Mamba2D.m. Modified from Key, K. W., Constable, S. C., & Weiss, C.
    J. (2006), Mapping 3D salt using the 2D marine magnetotelluric
    method: Case study from Gemini Prospect, Gulf of Mexico. Geophysics,
    71(1), B17–B27. DOI: `10.1190/1.2168007
    <https://doi.org/10.1190/1.2168007>`_.

    
