Anisotropic Conductivity
------------------------

MARE2DEM can model isotropic to triaxially anisotropic conductivity but
tilted or generally anisotropic media is not supported.  Switching
between the various types of anisotropy is easily accomplished using the
anisotropy menu in the Mamba2D model building assistant.  For most
modeling purposes, you will likely want to use simple isotropic
conductivity, but for modeling  a stack of interbedded marine sediments 
the transversely isotropic vertical (TIZ) option can be useful. The
other options are for more specialized anisotropies. In general if you
have no idea about anisotropy or which setting you should use,  go for
the isotropic default.

Here's a guide to the five possible options:

- **isotropic** - conductivity is the same in all direction of electric
  current flow. The conductivity tensor has the form: 

.. math::
    
    {{ \bar{ \sigma}}}= \left[\begin{array}{ccc}\sigma & 0 & 0 \\0
    & \sigma &0\\ 0 & 0 & \sigma \end{array}\right] . 


- **triaxial** - conductivity varies in the direction of the three coordinate
  axes. The conductivity tensor has the form:

.. math::    
    {\mathbf{ \bar{ \sigma}}} = \left[\begin{array}{ccc}\sigma_{x} & 0 & 0
    \\0 & \sigma_{y} &0\\ 0 & 0 & \sigma_{z} \end{array}\right], 

where  *x,y,z* are the 2D model axes.

There are three other possible anisotropies where the conductivity along
one axis is different than the other two. This type of anisotropy is
generally referred to as transverse isotropy,  where conductivity is
symmetric about an axis normal to the plane of isotropy.   MARE2DEM uses
the abbreviation TI  for transverse isotropy and the third letter
denotes the transverse axis. The three possible cases are:

- **TIX** - transversely isotropic perpendicular to the x axis:

.. math::     
    {\mathbf{ \bar{ \sigma}}} = \left[\begin{array}{ccc}\sigma_{\|} & 0 & 0
    \\0 & \sigma_{\bot} &0\\ 0 & 0 & \sigma_{\bot} \end{array}\right],

where :math:`\sigma_{\|}`  denotes conductivity along the *x* axis, and
:math:`\sigma_{\bot}`  is conductivity in the transverse plane.

- **TIY** - transversely isotropic  perpendicular to the *y* axis:
  
.. math::   

    {\mathbf{ \bar{ \sigma}}} = \left[\begin{array}{ccc}\sigma_{\bot} & 0 &
    0 \\0 & \sigma_{\|} &0\\ 0 & 0 & \sigma_{\bot} \end{array}\right]. 

- **TIZ** - transversely isotropic  perpendicular to the *z* axis:

.. math::
  
     {\mathbf{ \bar{ \sigma}}} = \left[\begin{array}{ccc}\sigma_{\bot} & 0 &
    0 \\0 & \sigma_{\bot} &0\\ 0 & 0 & \sigma_{\|} \end{array}\right]. 

This is also known in the exploration community as vertically
transverse isotropy (VTI).



