.. _sect_command_line_args: 

Command Line Arguments
----------------------

Normally MARE2DEM is called with just the name of the input resistivity file:

    ``mpirun -n <np> MARE2DEM <resistivity file>``

where ``<resistivity file>`` is the name of the required input
:Ref:`sect_resistivity_file`. By convention, this file should have the extension
``.resistivity``. For example ``inputModel.0.resistivity``.  The model found by
each inversion iteration is then output to a new resistivity file with
the iteration number incremented.  For example:
``inputModel.1.resistivity``, ``inputModel.2.resistivity``, ... The
corresponding model responses are written to ``inputModel.1.resp``,
``inputModel.2.resp``,...  

The command ``mpirun`` starts up the parallel message
passing interface environment and has argument ``-n <np>`` where ``<np>`` should be
the number of processing cores on your system (laptop, desktop, etc).
 
For inversion models, MARE2DEM will also outputs the normalized data
sensitivities to the free parameters (see :Ref:`sect_sensitivity`).  The
sensitivity is output to the file ``<resistivity
file>.<iter#>.sensitivity``. plotMARE2DEM.m can be used to plot
the sensitivity; it can be overlain as contours or shading on inversion
models. The sensitivity can be used as a relative measure of data
sensitivity to structure, but be aware this is a linearized
approximation for what is a non-linear inverse problem, so results
should be taken with a grain of salt.
      
MARE2DEM has a few other command line options for more advanced usage, but most users 
won't need to use these:
  
    ``mpirun -n <np>  MARE2DEM [-F] [-J] [-scratch <scratchfolder>] <resistivity file> [<output file>]``
   
where the arguments in brackets [] are optional:

    ``<output file>`` - With this option, the output files are 
    named ``<output file>.1.resistivity``, ``<output file>.1.resp``,  
    ``<output file>.2.resistivity``, ``<output file>.2.resp``,... 

    ``-F`` Computes the forward response of the input model only. The
    forward response is output to ``<resistivity file>.resp``. Note that
    if MARE2DEM detects no free parameters in the input model it will
    only compute the forward response; thus, typically the ``-F`` is
    only required when you want to compute the forward response of an
    inversion model (say when doing sensitivity tests of anomalous
    features found by inversion, where you paint over the anomaly using
    Mamba2D and then see how the MT or CSEM responses change).


    ``-J`` Outputs the full Jacobian matrix of the input model for each
    iteration. This flag is provided for advanced users wishing to use
    the Jacobian matrix in other calculations and normal users do not
    need to use this; further, the output Jacobian files can be quite
    large. The Jacobian matrix is written to an unformatted binary data
    file columnwise with the naming convention ``<resistivity
    file>.<iter#>.jacobianBin``. Note that the saved J is unweighted
    (i.e. it has NOT been normalized by the data uncertainties).  

    ``-scratch <scratchfolder>``  Use the specified directory for the scratch files 
    required for 2.5D CSEM inversion (but not needed for MT). Optimally     
    this should be a local directory on each compute node and not
    a networked directory.      
 
 
 
  


