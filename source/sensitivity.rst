.. _sect_sensitivity:

Normalized Sensitivity
======================

In addition to saving a :Ref:`sect_resistivity_file` for each inversion
iteration, MARE2DEM will save a ``<model name>.<iteration
#>.sensistivity`` file that has the normalized data sensitivity computed
by summing the Jacobian matrix columnwise over all *n* data:

 .. math ::
 
    S_j = \frac{1}{A_j} \sum_i^n | W_{ii} J_{ij} |
   
where 
        
.. math ::        
    
    J_{ij} = \frac{\partial d_i}{\partial m_j},
    
:math:`W_{ii}` is the inverse standard error for the *i*\ th datum, and
:math:`A_j` is the area of the *j*\ th free parameter. The normalization
by the parameter area decouples the sensitivity from the size of each
parameter. Normalize  *S* has units log10(S/m)/m\ :sup:`2`.
    
The sensitivity can be used as a relative measure of data
sensitivity to structure, but be aware this is a linearized
approximation for what is a non-linear inverse problem, so sensitivity 
plots should be taken with a grain of salt.
