.. _sect_penalty_matrix: 

Model Roughness 
===============

Spatial Gradient Roughness Operator
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

The gradient roughness operator output by Mamba2D in the
:Ref:`sect_penalty_file` approximates the integral of the model
spatial gradient over each parameter using the formula

.. math::
     \|     \mathbf{R} \mathbf{m}   \|^{2}   = \sum_{i=1}^{m}  \left (
    \sum_{j=1}^{N(i)} \left [ w_j \left( m_i -  m_j \right) \right ]^2 
    \right ) 

where :math:`N(i)` is the number of neighboring parameters around
parameter :math:`\mathbf{m}_i` and :math:`w_j` are weights set so
that the sum of differences approximates the integral of the local
gradient. Note that the parameter indices correspond to the parameter
numbers in the :ref:`sect_resistivity_file`, not the region numbers
since each region can have fixed or free parameters, and because 
each region will have more than one parameter for  anisotropic
models. See  [Key16]_ for more details.

Minimum Gradient Support Roughness Operator 
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
(to do)

Anisotropy Roughness Operator
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

For anisotropic models, the roughness is augmented by splitting the
model vector into anisotropic subsets  

.. math::
    \mathbf{m} = 
    \left [
        \begin{matrix}
            \mathbf{m}_x \\
            \mathbf{m}_y \\
            \mathbf{m}_z 
        \end{matrix}
    \right]


so that 

.. math::
     \|      \mathbf{R} \mathbf{m}   \|^{2}  \equiv  \|     \mathbf{R}
    \mathbf{m}_x  \|^{2} + \|     \mathbf{R} \mathbf{m}_y   \|^{2} +  \|
    \mathbf{R}\mathbf{m}_z   \|^{2} + \alpha \|   \mathbf{m}-\mathbf{m}'   
    \|^{2}


where the last term on the right is used to  penalize anisotropy and can
be arbitrarily dialed up or down with the scalar parameter :math:`\alpha`` (also referred to as the 
anisotropy penalty weight), and the permutation of model vector

.. math::
    \mathbf{m}' = \left [
    \begin{matrix}
        \mathbf{m}_y \\
        \mathbf{m}_z \\
        \mathbf{m}_x 
    \end{matrix}
    \right].
 
For the case of transversely isotropic models, there are only two
anisotropic components instead of the three components shown above.



