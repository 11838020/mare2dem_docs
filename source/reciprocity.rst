
.. _sect_reciprocity:

Electromagnetic Reciprocity
---------------------------

 .. 
    For CSEM modeling, each transmitter to be modeled results in a column
    vector for the right-hand side of the linear system. Thus if there are
    :math:`n_{tx}` transmitters, then :math:`n_{tx}` linear systems to
    solve. MARE2DEM uses a sparse matrix factorization routine to first
    factor the matrix (the most time consuming part) and then the solution
    to  right hand side column vector