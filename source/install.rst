
Installation   
=============

.. Note :: 
    While MARE2DEM is a highly capable modeling code, it is not a
    plug-and-play GUI software package. The intended user base
    includes research geophysicists and students with some
    familiarity with MATLAB and Unix commands. 
   
System Requirements
-------------------

  - **Intel Fortran and C and Fortran compilers** and the Math Kernel Library
  - An **MPI compiler** library built around the Intel Fortran and C compilers
  - **MATLAB** is needed for the graphical user interface (version R2014b or newer) 
    
MARE2DEM was  designed to be run in parallel on Unix based high
performance computing systems but it can also be run on Unix based
laptops and desktops.  The  source codes consist primarily of *modern*
Fortran subroutines but also a few C routines and libraries. The
parallel constructs are implemented with the message passing interface
(MPI) standard using a manager-worker model. Compiling the code on any
Unix based system (HPC cluster, desktop or laptop) is straightforward
using the included Makefile. The user interface is run locally using
MATLAB on your desktop or laptop.

At present, MARE2DEM requires the Intel compilers. If you don't have
them already, you can visit the Intel website and download a free trial
of their compilers. This gives you the ``ifort`` and ``icc`` compiler
executables.  You can download Open-MPI for free from `openmpi.org
<http://openmpi.org>`_. First install the Intel compilers, then follow
the Open-MPI instructions for compiling the MPI wrappers around ifort
and icc (for example see :ref:`sect_macos_openmpi`).  
On some clusters, we've had success building MARE2DEM with
other MPI implementations such as MPICH. 

.. Note:: 
    With some effort, it would be possible to compile the source code using
    ``gfortran`` and ``gcc`` and the free version of the Intel Math Kernel
    Library, which is used for both LAPACK and the Pardiso direct sparse
    matrix solver. Alternatively, the code could be modified to only use the
    free SuperLU solver library. If you get the code working with
    ``gfortran``, please consider contributing your modifications back to
    the community.

.. Warning::
    Building the code on a Windows system might be possible, but will require also 
    compiling the included ScaLAPACK 
    library.  The current MARE2DEM developer team does not have Windows expertise, 
    so please let us know if you figure out how to get it working on Windows.


MARE2DEM Source Code
--------------------
The source code resides in directory ``mare2dem_source`` and can be
compiled using the included ``Makefile``. The specific compiler
arguments need to be passed to the Makefile using an include file. The
subdirectory ``mare2dem_source/include/`` has example include files:

- ``macos.inc``  - settings for  MacOS systems with the Intel compilers
  and OpenMPI. 
 
- ``habanero.inc`` - settings for the Habanero cluster at
  Columbia University. This is a Linux cluster with the Intel Parallel
  Studio compiler suite (which includes MPI). It is likely that this
  include file will work on other Linux clusters with the Intel compilers.

See the include files for further details. 

To compile MARE2DEM, open a terminal and ``cd`` to 
``mare2dem_source/``. Then issue the ``make`` command with the argument
``INCLUDE=<include_file>``. For example, on the Habanero cluster, we
use:

.. code-block:: bash

    make INCLUDE=./include/habanero.inc
    
It will take several minutes to compile the libraries and MARE2DEM source code.  
For furter details, see :ref:`sect_habanero`.

.. Note::
    If you are testing out different compiler
    settings and want to clean up any object files from previous make attempts, 
    you can use the command:

    .. code-block:: bash

        make clean_all 


   
MATLAB User Interface
--------------------- 

    
Open MATLAB and click on the ``Set Path`` button on the ``Home`` tab of the toolbar. Click on the 
``Add with Subfolders...`` button and select the folder ``mare2dem_matlab`` then click on ``Save``. You 
should see several folders added like in the figure below.

.. figure::  _static/images/matlab_set_path.png 
    :align: center
    :figwidth: 700px

    Adding the MARE2DEM interface and plotting routines to MATLAB's path.

.. Note:: 
    The user interface routines were originally written in MATLAB because it has easy tools for making interactive
    graphical interfaces. We hope to some day replace them with a new interface written in an 
    open-source and freely available language, but at the moment neither Python nor Julia have comparable interactive
    graphics capabilities that are as easy to program as those in MATLAB. QT is an option that is
    freely available but it's significantly more complicated to code up; however, if you have ideas and want to 
    make an open-source GUI for MARE2DEM, let's talk! 


