#! /bin/zsh

# this script could build and install both html and pdf:
make clean # just in case, start from a clean slate

rm -fR ../mare2dem.bitbucket.io/*

# Build latex pdf:
#make latexpdf

# copy pdf to ../mare2dem.bitbucket.io/:
#cp -R ./_build/latex/mare2dem.pdf ../mare2dem.bitbucket.io/



# build website:
make html

# install in bitbucket repo local copy:
cp -R ./_build/html/ ../mare2dem.bitbucket.io/

# commit and push to bitbucket:

# to do!



